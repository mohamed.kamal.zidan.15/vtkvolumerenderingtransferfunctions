#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "qvector.h"
#include <vtkCamera.h>
#include <vtkCommand.h>
#include <vtkCubeSource.h>
#include <vtkDataObjectToTable.h>
#include <vtkElevationFilter.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkQtTableView.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkSphereSource.h>
#include <vtkVersion.h>
#include <vtkFixedPointVolumeRayCastMapper.h>
#include <vtkVolumeProperty.h>
#include <vtkDICOMImageReader.h>
#include <vtkColorTransferFunction.h>
#include <vtkPiecewiseFunction.h>
#include <vtkImageData.h>

#if VTK_VERSION_NUMBER >= 89000000000ULL
#define VTK890 1
#endif

// Constructor
MainWindow::MainWindow(QWidget* parent)
  : QMainWindow(parent), ui(new Ui::MainWindow)
{
  this->ui->setupUi(this);

  vtkNew<vtkNamedColors> colors;

  vtkNew<vtkGenericOpenGLRenderWindow> renderWindowLeft;
#if VTK890
  this->ui->qvtkWidgetLeft->setRenderWindow(renderWindowLeft);
#else
  this->ui->qvtkWidgetLeft->SetRenderWindow(renderWindowLeft);
#endif

  // read DICOM series
  const char* folder = "E:/work-train/medsoft-repo/medsoft/TechMah/Resources/Crania";
  vtkNew<vtkDICOMImageReader> reader;
  reader->SetDirectoryName(folder);
  reader->Update();

  // CALCULATE HISTOGRAM	
  vtkImageData* imgs = reader->GetOutput();
 /* qDebug() << reader->GetHeight() << " , " <<
	  reader->GetWidth() << " , " <<
	  imgs->GetScalarRange()[0] << " , " <<
	  imgs->GetScalarRange()[1] << " , ";*/

  // 2POINT TO DEFINE OPACITY LINE
  int x1 = imgs->GetScalarRange()[0];
  int y1 = 0;
  int x2  = imgs->GetScalarRange()[1];
  int y2 = 101000;


  // vectors for histogram 
  QVector<double> hist;
  QVector<double> pins;

  
  // pins of the histogram
  for (double i = x1; i <= x2; i ++)
  {
	  pins.push_back(i);
	  hist.push_back(0);
  }
  
  // volume dimension
  int * dim = imgs->GetDimensions();

  // CALCULATE HISTOGRAM
  for (int x = 0; x < dim[0]; x++)
  {
	  for (int y = 0; y < dim[1]; y++)
	  {
		  for (int z = 0; z < dim[2]; z++)
		  {
			  double data = imgs->GetScalarComponentAsDouble(x, y, z, imgs->POINT) - x1;
			  hist[(int)data]++;
		  }
	  }
  }
 
  //y2 = *std::max_element(hist.constBegin(), hist.constEnd());
  // SLOPE OF OPACITY LINE
  double m = (y2 - y1) / (double)(x2 - x1);

  // add first point for the opacity function 
  opacityFun->AddPoint(x1,y1);

  // the opacity line
  QVector<double> xd;
  QVector<double> yd;


  // add first point for the opacity line
  xd.push_back(x1);
  yd.push_back(y1);

  // intermediate pointds
  int step = (x2 - x1) / 10;
  for (int i = (x1 + step); i < (x2 - step); (i += step))
  {
	 xd.push_back(i);
	int d = ((i - x1) * m) + y1;
	  yd.push_back(d);
	  opacityFun->AddPoint(i, (d/y2));

  }
  // add last point
  opacityFun->AddPoint(x2, 1);
  xd.push_back(x2);
  yd.push_back(y2);

  // plot the opacity line
  ui->HistPlot->addGraph()->addData(xd, yd);
  // plot the histogram
  ui->HistPlot->addGraph()->addData(pins, hist);
  ui->HistPlot->graph(1)->setBrush(QBrush(Qt::gray));

  ui->HistPlot->xAxis->setRange(x1, x2);
  ui->HistPlot->yAxis->setRange(y1, 103000 );

  ui->HistPlot->setDragInfo(0, 20); // data of graph(0) is dragable, 20px margin
  ui->HistPlot->replot();
  connect(ui->HistPlot, &EQWidget::DataChanged, this, &MainWindow::ModifiedHandler);

  //#####################
  // create volume and mapper
  vtkNew<vtkVolume> volume;
  vtkNew<vtkFixedPointVolumeRayCastMapper> mapper;

  mapper->SetInputConnection(reader->GetOutputPort());
  volume->SetMapper(mapper);



  // Create the property and attach the transfer functions
  vtkNew<vtkVolumeProperty> property;
  property->SetScalarOpacity(opacityFun);
  property->SetInterpolationTypeToLinear();
  volume->SetProperty(property);

  // renderer
  vtkNew<vtkRenderer> leftRenderer;
  leftRenderer->SetBackground(colors->GetColor3d("LightSteelBlue").GetData());

  // Add the volume to the scene
  leftRenderer->AddVolume(volume);


  // VTK/Qt wedded
#if VTK890
  this->ui->qvtkWidgetLeft->renderWindow()->AddRenderer(leftRenderer);
#else
  this->ui->qvtkWidgetLeft->GetRenderWindow()->AddRenderer(leftRenderer);
#endif

  leftRenderer->ResetCamera();
  ui->qvtkWidgetLeft->renderWindow()->Render();

  // Set up action signals and slots
  connect(this->ui->actionExit, SIGNAL(triggered()), this, SLOT(slotExit()));


}

void MainWindow::ModifiedHandler()
{
	ui->HistPlot->replot();
	auto data = ui->HistPlot->graph(0)->data();
	int size = data->size();
	// recalculate opacity function
	opacityFun->RemoveAllPoints();
	for (int i = 0; i < size; i++)
	{
		double x = data->at(i)->key;
		double y = data->at(i)->value;
		if (y >= 101000)
			y = 1;
		else if (y <= 0)
			y = 0;
		else
			y /= 101000;
		opacityFun->AddPoint(x, y);
	}
	ui->qvtkWidgetLeft->renderWindow()->Render();
}

void MainWindow::slotExit()
{
  qApp->exit();
}
