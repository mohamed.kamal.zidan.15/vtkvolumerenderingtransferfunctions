#ifndef MainWindow_H
#define MainWindow_H

#include <QMainWindow>
#include <vtkPiecewiseFunction.h>
#include <vtkNew.h>
/*
 * See "The Single Inheritance Approach" in this link:
 * [Using a Designer UI File in Your C++
 * Application](https://doc.qt.io/qt-5/designer-using-a-ui-file.html)
 */
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT
public:
  // Constructor/Destructor
  explicit MainWindow(QWidget* parent = nullptr);
  virtual ~MainWindow() = default;
private:
  // Designer form
  vtkNew<vtkPiecewiseFunction> opacityFun;
  Ui::MainWindow *ui;

public slots:
  void slotExit();

protected:
  void ModifiedHandler();
};

#endif
